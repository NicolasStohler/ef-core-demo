﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ConsoleApp.SQLite
{
    public class Program
    {
        public static void Main()
        {
            using (var db = new BloggingContext())
            {
                // AddRecords(db);

                DisplayCounts(db);

                //DeleteSingleByImportId(db, 4);
                //DeleteSingleByBlogId(db, 2);
                
                DisplayCounts(db);

                //// remove all
                //var allRoots = db.Imports.ToList();
                //db.Imports.RemoveRange(allRoots);
                //db.SaveChanges();

                DisplayBlogs(db);

                //var allPosts = db.Posts.ToList();
                //foreach (var post in allPosts)
                //{
                //    Console.WriteLine($"p = {post.Title}");
                //}

                Console.ReadLine();
            }
        }

        private static void DeleteSingleByImportId(BloggingContext db, int id)
        {
            // this removes the import and everything below!

            Console.WriteLine("remove single IMPORT");
            // delete single blog 4

            var b4 = db.Imports
                .First(b => b.ImportId == id);

            db.Imports.Remove(b4);
            db.SaveChanges();
        }

        private static void DeleteSingleByBlogId(BloggingContext db, int id)
        {
            // this will remove the blog and everything below, but NOT the import!

            Console.WriteLine("remove single BLOG");
            // delete single blog 4

            var b4 = db.Blogs
                .First(b => b.BlogId == id);

            db.Blogs.Remove(b4);
            db.SaveChanges();
        }

        private static void DisplayCounts(BloggingContext db)
        {
            Console.WriteLine("Counts:");
            Console.WriteLine($"  - Imports   : {db.Imports.Count()}");
            Console.WriteLine($"  - Blogs     : {db.Blogs.Count()}");
            Console.WriteLine($"  - BlogInfos : {db.BlogInfos.Count()}");
            Console.WriteLine($"  - Posts     : {db.Posts.Count()}");
        }

        private static void DisplayBlogs(BloggingContext db)
        {
            Console.WriteLine();
            Console.WriteLine("All blogs in database:");
            var loadedBlogs = db.Blogs
                .Include(b => b.Posts)
                .Include(b => b.BlogInfo)
                .ToList();

            foreach (var blog in loadedBlogs)
            {
                Console.WriteLine($" - {blog.Url} - {blog.BlogId}");
                Console.WriteLine($" + {blog.BlogInfo.Text}");
                foreach (var post in blog.Posts)
                {
                    Console.WriteLine("   = {0}", post.Title);
                }
            }
        }

        private static void AddRecords(BloggingContext db)
        {
            for (int i = 0; i < 5; i++)
            {
                //db.Blogs.Add(new Blog
                //{
                //    Url = "http://blogs.msdn.com/adonet",
                //    Posts = new List<Post>()
                //    {
                //        new Post() {Title = "aaa", Content = "text aaa"},
                //        new Post() {Title = "bbb", Content = "text bbb"},
                //        new Post() {Title = "ccc", Content = "text ccc"},
                //    },
                //    BlogInfo = new BlogInfo()
                //    {
                //        Text = "bla bla"
                //    },
                //    ReverseInfo = new ReverseInfo()
                //    {
                //        ReverseText = "alb alb alb ;)"
                //    }
                //});
                db.Imports.Add(new Import()
                {
                    Name = "import ;)",
                    Blog = new Blog
                    {
                        Url = "http://blogs.msdn.com/adonet",
                        Posts = new List<Post>()
                        {
                            new Post() {Title = "aaa", Content = "text aaa"},
                            new Post() {Title = "bbb", Content = "text bbb"},
                            new Post() {Title = "ccc", Content = "text ccc"},
                        },
                        BlogInfo = new BlogInfo()
                        {
                            Text = "bla bla"
                        },
                    }
                });
            }

            var count = db.SaveChanges();
            Console.WriteLine("{0} records saved to database", count);
        }
    }
}