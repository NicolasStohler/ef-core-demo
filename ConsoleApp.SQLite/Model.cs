﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp.SQLite
{
    public class BloggingContext : DbContext
    {
        public DbSet<Import>   Imports   { get; set; }
        public DbSet<Blog>     Blogs     { get; set; }
        public DbSet<BlogInfo> BlogInfos { get; set; }
        public DbSet<Post>     Posts     { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=blogging.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Import>()
                .HasOne(i => i.Blog)
                .WithOne(b => b.Import)
                .HasForeignKey<Blog>(b => b.ImportId)
                ;

            modelBuilder.Entity<Blog>()
                .HasOne(b => b.BlogInfo)
                .WithOne(bi => bi.Blog)
                .HasForeignKey<BlogInfo>(b => b.BlogId)
                ;


            //modelBuilder.Entity<ReverseInfo>()
            //    .HasOne(r => r.Blog)
            //    .WithOne(b => b.ReverseInfo)
            //    .HasForeignKey<Blog>(b => b.BlogId) // NOT b.BlogId!!!
            //    ;
        }
    }

    public class Import
    {
        public int    ImportId { get; set; }
        public string Name     { get; set; }

        // public int  BlogId { get; set; }
        public Blog Blog { get; set; }
    }

    public class Blog
    {
        public int    BlogId { get; set; }
        public string Url    { get; set; }

        public ICollection<Post> Posts { get; set; }

        //public int      BlogInfoId { get; set; }
        public BlogInfo BlogInfo { get; set; }

        //public int         ReverseInfoId { get; set; }
        //public ReverseInfo ReverseInfo   { get; set; }

        public int    ImportId { get; set; }
        public Import Import   { get; set; }
    }

    public class BlogInfo
    {
        public int BlogInfoId { get; set; }

        public string Text { get; set; }

        public int  BlogId { get; set; }
        public Blog Blog   { get; set; }
    }

    //public class ReverseInfo
    //{
    //    public int ReverseInfoId { get; set; }

    //    public string ReverseText { get; set; }

    //    //public int BlogId { get; set; }
    //    public Blog Blog { get; set; }
    //}

    public class Post
    {
        public int    PostId  { get; set; }
        public string Title   { get; set; }
        public string Content { get; set; }

        public int  BlogId { get; set; }
        public Blog Blog   { get; set; }
    }
}