﻿// <auto-generated />
using ConsoleApp.SQLite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ConsoleApp.SQLite.Migrations
{
    [DbContext(typeof(BloggingContext))]
    partial class BloggingContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024");

            modelBuilder.Entity("ConsoleApp.SQLite.Blog", b =>
                {
                    b.Property<int>("BlogId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ImportId");

                    b.Property<string>("Url");

                    b.HasKey("BlogId");

                    b.HasIndex("ImportId")
                        .IsUnique();

                    b.ToTable("Blogs");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.BlogInfo", b =>
                {
                    b.Property<int>("BlogInfoId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogId");

                    b.Property<string>("Text");

                    b.HasKey("BlogInfoId");

                    b.HasIndex("BlogId")
                        .IsUnique();

                    b.ToTable("BlogInfos");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.Import", b =>
                {
                    b.Property<int>("ImportId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ImportId");

                    b.ToTable("Imports");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.Post", b =>
                {
                    b.Property<int>("PostId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogId");

                    b.Property<string>("Content");

                    b.Property<string>("Title");

                    b.HasKey("PostId");

                    b.HasIndex("BlogId");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.Blog", b =>
                {
                    b.HasOne("ConsoleApp.SQLite.Import", "Import")
                        .WithOne("Blog")
                        .HasForeignKey("ConsoleApp.SQLite.Blog", "ImportId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ConsoleApp.SQLite.BlogInfo", b =>
                {
                    b.HasOne("ConsoleApp.SQLite.Blog", "Blog")
                        .WithOne("BlogInfo")
                        .HasForeignKey("ConsoleApp.SQLite.BlogInfo", "BlogId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ConsoleApp.SQLite.Post", b =>
                {
                    b.HasOne("ConsoleApp.SQLite.Blog", "Blog")
                        .WithMany("Posts")
                        .HasForeignKey("BlogId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
