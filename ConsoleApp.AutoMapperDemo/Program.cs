﻿using System;
using AutoMapper;

namespace ConsoleApp.AutoMapperDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<XmlDosimeter, DosimeterBase>()
                    .ForMember(d => d.Dose, o => o.MapFrom(s => s.Dose * 2.0))
                    .IncludeAllDerived()  // if this is commented out, the above mapping will not take place in derived mappings
                    ;

                cfg.CreateMap<XmlDosimeter, WholeBodyDosimeter>()
                    .ForMember(d => d.WbText, o => o.MapFrom(s => s.Text))
                    ;
                cfg.CreateMap<XmlDosimeter, NeutronBodyDosimeter>()
                    .ForMember(d => d.NeutronText, o => o.MapFrom(s => s.Text))
                    ;

                cfg.CreateMap<XmlContainer, DbContainer>()
                    .ForMember(d => d.WholeBodyDosimeter, o => o.MapFrom(s => s.XmlDosimeterWholeBody))
                    .ForMember(d => d.NeutronBodyDosimeter, o => o.MapFrom(s => s.XmlDosimeterNeutron))
                    ;
            });

            var xmlDosimeter = new XmlDosimeter() {Dose = 1.23};

            var m = Mapper.Map<WholeBodyDosimeter>(xmlDosimeter);

            Console.WriteLine($"m is {m.GetType().Name}, dose = {m.Dose}");

            var m2 = Mapper.Map<NeutronBodyDosimeter>(xmlDosimeter);

            Console.WriteLine($"m2 is {m2.GetType().Name}, dose = {m2.Dose}");

            var container = new XmlContainer()
            {
                XmlDosimeterWholeBody = new XmlDosimeter() {Dose = 1.11, Text = "wb"},
                XmlDosimeterNeutron   = new XmlDosimeter() {Dose = 6.66, Text = "ne"},
            };

            var c = Mapper.Map<DbContainer>(container);
            Console.WriteLine($"c is {c.GetType().Name}");
            Console.WriteLine($"  wb: {c.WholeBodyDosimeter.Dose}, {c.WholeBodyDosimeter.WbText}");
            Console.WriteLine($"  neutron: {c.NeutronBodyDosimeter.Dose}, {c.NeutronBodyDosimeter.NeutronText}");
        }
    }

    public class XmlContainer
    {
        public XmlDosimeter XmlDosimeterWholeBody { get; set; }
        public XmlDosimeter XmlDosimeterNeutron   { get; set; }
    }

    public class XmlDosimeter
    {
        public double Dose { get; set; }
        public string Text { get; set; }
    }

    //---

    public class DbContainer
    {
        public WholeBodyDosimeter   WholeBodyDosimeter   { get; set; }
        public NeutronBodyDosimeter NeutronBodyDosimeter { get; set; }
    }

    public class DosimeterBase
    {
        public double Dose { get; set; }
    }

    public class WholeBodyDosimeter : DosimeterBase
    {
        public string WbText { get; set; }
    }

    public class NeutronBodyDosimeter : DosimeterBase
    {
        public string NeutronText { get; set; }
    }
}